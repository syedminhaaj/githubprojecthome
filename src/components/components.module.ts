import { NgModule } from '@angular/core';
import { SearchResultsComponent } from './search-results/search-results.component';
@NgModule({
	declarations: [SearchResultsComponent],
	imports: [],
	exports: [SearchResultsComponent]
})
export class ComponentsModule {}
